package fr.ulille.iut.tva.ressource;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.tva.dto.DetailsDto;
import fr.ulille.iut.tva.dto.InfoTauxDto;
import fr.ulille.iut.tva.service.CalculTva;
import fr.ulille.iut.tva.service.TauxTva;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

/**
 * TvaRessource
 */
@Path("tva")
public class TvaRessource {
	@SuppressWarnings("unused")
	private CalculTva calculTva = new CalculTva();

	@GET
	@Path("tauxpardefaut")
	public double getValeurTauxParDefaut() {
		return TauxTva.NORMAL.taux;
	}
	
	@GET
	@Path("valeur/{niveauTva}")
	public double getValeurTaux(@PathParam("niveauTva") String niveau) {
		try {
            return TauxTva.valueOf(niveau.toUpperCase()).taux; 
        }
        catch ( Exception ex ) {
            throw new NiveauTvaInexistantException();
        }

	}
	
	@GET
	@Path("{niveauTva}")
	public double getMontantTotal(@PathParam("niveauTva") String niveau, @QueryParam("somme") double somme) {
		return (this.getValeurTaux(niveau)/100)*somme+somme;
	}
	
	@GET
	@Path("lestaux")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public List<InfoTauxDto> getInfoTaux() {
	  ArrayList<InfoTauxDto> result = new ArrayList<InfoTauxDto>();
	  for ( TauxTva t : TauxTva.values() ) {
	    result.add(new InfoTauxDto(t.name(), t.taux));
	  }
	  return result;
	}
	
	@GET
	@Path("details/{taux}")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public DetailsDto getDetail(@PathParam("taux") String taux, @QueryParam("somme") double valeur){
		taux = taux.toUpperCase();
		double montantTotal = this.getMontantTotal(taux, valeur);
		double montantTva = (this.getValeurTaux(taux)/100)*valeur;
		String tauxLabel = TauxTva.valueOf(taux).name();
		double tauxValue = TauxTva.valueOf(taux).taux;
		return new DetailsDto(montantTotal, montantTva, valeur, tauxLabel, tauxValue);
	}

}